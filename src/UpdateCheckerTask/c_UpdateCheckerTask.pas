unit c_UpdateCheckerTask;

interface

const
  // Do not change this values never!

  cGoogleEarthDesktopEathGUID     = '{2736BF1B-C010-4262-91DD-BFA152DAB1E1}';
  cGoogleEarthDesktopHistoryGUID  = '{FC49C486-B6CD-4FF2-B7A8-413EA1402695}';
  cGoogleEarthDesktopSkyGUID      = '{9D7D345D-F3F1-4287-8A3D-EE6AA3099BE8}';
  cGoogleEarthDesktopMarsGUID     = '{F5872C50-7F68-4B43-AED3-DB34A71B5FA4}';
  cGoogleEarthDesktopMoonGUID     = '{AC2DAEA2-8F2A-42FC-87E1-397DA2C203F3}';
  cGoogleEarthDesktopClientGUID   = '{146E4984-C081-4DA0-9F79-5855F271923C}';

  cGoogleEarthWebEarthGUID        = '{CABC52B6-A855-43D8-88CB-718A04ECF82F}';
  cGoogleEarthWebClientGUID       = '{E36D2679-DEA2-4E05-93D0-777DF1E9F913}';

  cGoogleMapsClassicEarthGUID     = '{D732DF07-37C4-4773-9C88-AA95C1A7AAFF}';
  cGoogleMapsClassicJSAPIGUID     = '{45B30A7C-F81D-4E85-9E7D-52DE35E25173}';

  cGoogleMapsEarthGUID            = '{34D0C112-755B-4D4F-B9A4-9F4961E5FA84}';
  cGoogleMapsMarsGUID             = '{A6359894-6FA9-4717-801A-4D011DC8AAAD}';
  cGoogleMapsMoonGUID             = '{42F97AF2-D938-4056-9F7B-738BDADA6CF8}';

implementation

end.
