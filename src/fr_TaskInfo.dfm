object frTaskInfo: TfrTaskInfo
  Left = 0
  Top = 0
  Width = 330
  Height = 13
  TabOrder = 0
  object lblName: TLabel
    Left = 60
    Top = 0
    Width = 4
    Height = 13
    Alignment = taRightJustify
  end
  object lblVersion: TLabel
    Left = 80
    Top = 0
    Width = 4
    Height = 13
  end
  object lblLastModified: TLabel
    Left = 140
    Top = 0
    Width = 4
    Height = 13
  end
  object lblNew: TLabel
    Left = 270
    Top = 0
    Width = 4
    Height = 13
  end
end
