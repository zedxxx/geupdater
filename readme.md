**GoogleEarth Update Checker** is a program that allows you to automatically check for changes in the data versions on the Google Earth and Google Maps servers.

![](doc/geupdater_1.png)

![](doc/geupdater_2.png)



